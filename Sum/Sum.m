//
//  Sum.m
//  Sum
//
//  Created by Ksendr on 18.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import "Sum.h"

@interface Sum()
    
@property int c;
@property int n;

@end

@implementation Sum

-(id)init:(NSString *)c :(NSString *)n{
    self = [super init];
    if(self){
        self.c = [self parse:c];
        self.n = [self parse:n];
        [self getRandomArray:self.n];
    }
    return self;
}

-(BOOL)find{
    self.arr = [self.arr sortedArrayUsingComparator:^(id obj1, id obj2){
        if (obj1 == obj2) return NSOrderedSame;
        return (obj1 < obj2) ? NSOrderedAscending : NSOrderedDescending;
    }];
    
    for(int i = self.n - 1; i >= 0; i--){
        int dif = self.c - (int)[self.arr[i] integerValue];
        if(dif >= 0){
            for (int j = 0; j < i; j++) {
                if((int)[self.arr[j] integerValue] == dif){
                    self.a = (int)[self.arr[i] integerValue];
                    self.b = (int)[self.arr[j] integerValue];
                    return YES;
                }
                else{
                    if((int)[self.arr[j] integerValue] > dif){
                        break;
                    }
                }
            }
        }
    }
    
    return NO;
}

-(NSString *)arrayToString{
    NSMutableString* str = [NSMutableString string];
    for(int i =0; i<self.n; i++){
        [str appendString:[NSString stringWithFormat:@"%@ ",self.arr[i]]];
    }
    return str;
}

-(void)getRandomArray:(int)n {
    id numbers[n];
    for (int x = 0; x < n; ++x)
        numbers[x] = [NSNumber numberWithInt:arc4random_uniform(10000)];
    self.arr = [NSArray arrayWithObjects:numbers count:n];
}

-(int)parse:(NSString *)str{
    return [str isEqualToString:@""] ? 0 : [str doubleValue];
}

@end
