//
//  ViewController.m
//  Sum
//
//  Created by Ksendr on 18.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import "ViewController.h"
#import "Sum.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tbC;
@property (weak, nonatomic) IBOutlet UITextField *tbN;
@property (weak, nonatomic) IBOutlet UILabel *lbA;
@property (weak, nonatomic) IBOutlet UILabel *lbB;
@property (weak, nonatomic) IBOutlet UITextView *tvArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnFindClick:(UIButton *)sender {
    Sum *sum = [[Sum alloc] init:self.tbC.text :self.tbN.text];
    self.tvArray.text = [sum arrayToString];
    
    if([sum find]){
        self.lbA.text = [NSString stringWithFormat:@"a: %d", sum.a];
        self.lbB.text = [NSString stringWithFormat:@"b: %d", sum.b];
    }
    else{
        self.lbA.text = @"a: not found";
        self.lbB.text = @"b: not found";

    }
}

@end
