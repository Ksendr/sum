//
//  Sum.h
//  Sum
//
//  Created by Ksendr on 18.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sum : NSObject

@property int a;
@property int b;
@property NSArray *arr;

-(id)init:(NSString *)c :(NSString *)n;
-(BOOL)find;
-(NSString *)arrayToString;

@end
