//
//  AppDelegate.h
//  Sum
//
//  Created by Ksendr on 18.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

